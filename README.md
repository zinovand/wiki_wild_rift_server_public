# wiki_wild_rift

Implementace wiki po hre "Leaque of Legends: wild rift"
Schematu databazi muzete najit v souboru "schemata.png" (cervenym oznaceny implementovany entity)
Postman instrukce k pouziti REST API jsou v souboru "requests.json"

P.S. Web client pro tento project: https://gitlab.fit.cvut.cz/zinovand/wiki_wild_rift_client_public

P.P.S. Ted' vim, ze pro nazvy promennych v Jave bylo by lip pouzit lowerCamelCase. 