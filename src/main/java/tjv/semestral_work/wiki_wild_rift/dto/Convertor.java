package tjv.semestral_work.wiki_wild_rift.dto;

import tjv.semestral_work.wiki_wild_rift.model.Character;
import tjv.semestral_work.wiki_wild_rift.model.ClassCharacter;
import tjv.semestral_work.wiki_wild_rift.model.Skill;

public class Convertor {

    public CharacterDTO toCharacterDTO(Character character) {
        CharacterDTO res = new CharacterDTO();
        res.setId(character.getId());
        res.setName(character.getName());
        res.setHealth(character.getHealth());
        res.setMove_speed(character.getMove_speed());
        res.setAttack_speed(character.getAttack_speed());
        res.setPhysical_damage(character.getPhysical_damage());
        res.setMagic_damage(character.getMagic_damage());
        res.setPhysical_armor(character.getPhysical_armor());
        res.setMagic_armor(character.getMagic_armor());
        res.setHero_class(character.get_class());
        res.setHero_skills(character.get_skills());
        res.setWeak_vs(character.get_weak_vs());
        res.setStrong_vs(character.get_strong_vs());
        return res;
    }

    public ClassDTO toClassDTO(ClassCharacter classCharacter) {
        ClassDTO res = new ClassDTO();
        res.setId(classCharacter.getId());
        res.setName(classCharacter.getName());
        res.setHeroes(classCharacter.get_heroes());
        return res;
    }

    public SkillDTO toSkillDTO(Skill skill) {
        SkillDTO res = new SkillDTO();
        res.setId(skill.getId());
        res.setName(skill.getName());
        res.setEffect(skill.getEffect());
        res.setCost(skill.getCost());
        res.setCooldown(skill.getCooldown());
        res.setAction_time(skill.getAction_time());
        res.setIs_passive(skill.getIs_passive());
        res.setOwner(skill.get_owner());
        return res;
    }

//    public static String asJsonString(final Object obj) {
//        try {
//            final ObjectMapper mapper = new ObjectMapper();
//            final String jsonContent = mapper.writeValueAsString(obj);
//            return jsonContent;
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }

}
