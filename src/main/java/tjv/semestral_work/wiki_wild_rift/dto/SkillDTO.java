package tjv.semestral_work.wiki_wild_rift.dto;

import com.fasterxml.jackson.annotation.JsonView;

public class SkillDTO {
    @JsonView
    public Integer id;

    @JsonView
    public String name;

    @JsonView
    public String effect;

    @JsonView
    public int action_time;

    @JsonView
    public int cooldown;

    @JsonView
    public int cost;

    @JsonView
    public Boolean is_passive;

    @JsonView
    public String owner;

    public SkillDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public int getAction_time() {
        return action_time;
    }

    public void setAction_time(int action_time) {
        this.action_time = action_time;
    }

    public int getCooldown() {
        return cooldown;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public Boolean getIs_passive() {
        return is_passive;
    }

    public void setIs_passive(Boolean is_passive) {
        this.is_passive = is_passive;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
