package tjv.semestral_work.wiki_wild_rift.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tjv.semestral_work.wiki_wild_rift.model.ClassCharacter;
import tjv.semestral_work.wiki_wild_rift.model.Skill;

import java.util.List;
import java.util.Set;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Integer> {

    Boolean existsByName(String Name);

    Boolean existsByEffect(String Effect);

    Skill getByName(String Name);

    @Modifying
    @Query(value = "UPDATE Skill s SET s.CHARACTER = (SELECT c FROM Character c WHERE c.id_character = :id_owner) WHERE s.id_skill = :id_skill")
    void updateOwnerById(Integer id_skill, Integer id_owner);

    @Modifying
    @Query("UPDATE Skill SET name = :name, effect = :effect, action_time = :at, cost = :cost, cooldown = :cd, is_passive = :passive WHERE id_skill = :id")
    void updateById(Integer id, String name, String effect, Integer at, Integer cost, Integer cd, Boolean passive);

    @Modifying
    @Query(value = "SELECT s FROM Skill s WHERE s.CHARACTER.id_character IN (SELECT c.id_character FROM Character c WHERE c.CLASS.id_class IN (SELECT cl.id_class FROM ClassCharacter cl WHERE cl.name = :class_name ))")
    List<Skill> getAllSkillForClass(String class_name);

    // @Query(value = "SELECT s FROM Skill s WHERE s.name = :class_name")
    //@Query(value = "SELECT s FROM Skill s JOIN Character c WHERE c.id_character=s.CHARACTER.id_character")
    //@Query(value = "SELECT s FROM Skill s WHERE s.CHARACTER.id_character IN (SELECT id_character from Character WHERE Character.CLASS.name = :class_name)")
}
