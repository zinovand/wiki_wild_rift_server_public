package tjv.semestral_work.wiki_wild_rift.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tjv.semestral_work.wiki_wild_rift.model.Character;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Integer> {

    Boolean existsByName(String Name);

    Character getByName(String Name);

    @Modifying
    @Query(value = "UPDATE Character ch SET ch.CLASS = (SELECT cc FROM ClassCharacter cc WHERE cc.id_class = :id_class) WHERE ch.id_character = :id_character")
    void updateClassById(Integer id_character, Integer id_class);

    @Modifying
    @Query("UPDATE Character SET name = :name, health = :hp, move_speed = :ms, attack_speed = :as, physical_damage = :pd, magic_damage = :md, physical_armor = :pa, magic_armor = :ma WHERE id_character = :id")
    void updateById(Integer id, String name, Integer hp, Integer ms, Double as, Integer pd, Integer md, Integer pa, Integer ma);

    @Modifying
    @Query(value = "UPDATE Skill s SET s.CHARACTER = null WHERE s.CHARACTER.id_character = :id_character")
    void deleteLinksSkill(Integer id_character);

}
