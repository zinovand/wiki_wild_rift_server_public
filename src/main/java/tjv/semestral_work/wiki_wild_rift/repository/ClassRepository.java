package tjv.semestral_work.wiki_wild_rift.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tjv.semestral_work.wiki_wild_rift.model.ClassCharacter;

@Repository
public interface ClassRepository extends JpaRepository<ClassCharacter, Integer> {

    Boolean existsByName(String Name);

    ClassCharacter getByName(String Name);

    @Modifying
    @Query("UPDATE ClassCharacter SET name = :name WHERE id_class = :id")
    void updateById(Integer id, String name);

    @Modifying
    @Query(value = "UPDATE Character c SET c.CLASS = null WHERE c.CLASS.id_class = :id_class")
    void deleteLinks(Integer id_class);
}
