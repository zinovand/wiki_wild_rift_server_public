package tjv.semestral_work.wiki_wild_rift.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

@Entity
public class ClassCharacter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_class;

    @Column(nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "CLASS")
    private List<Character> CHARACTERS;

    public ClassCharacter() {
    }

    public ClassCharacter(String name) {
        this.name = name;
    }

    public int getId() {
        return id_class;
    }

    public void setId(Integer id) {
        this.id_class = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String get_heroes() {
        String res_empty = "No characters with this class id DB";
        String res = "";
        if (CHARACTERS != null) {
            int i = CHARACTERS.size();
            if (i == 0) {
                return res_empty;
            }
            for (Character each : CHARACTERS) {
                if (i == 1) {
                    res += each.getName();
                } else {
                    res += each.getName() + ", ";
                }
                i--;
            }
        } else {
            return res_empty;
        }
        return res;
    }
}
