package tjv.semestral_work.wiki_wild_rift.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
public class Skill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_skill;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private String effect;

    @Column
    private int action_time;

    @Column
    private int cooldown;

    @Column
    private int cost;

    @Column(nullable = false)
    private Boolean is_passive;

    @ManyToOne
    private Character CHARACTER;

    public Skill() {
    }

    public Skill(String name, String effect, int action_time, int cooldown, int cost, boolean is_passive) {
        this.name = name;
        this.effect = effect;
        this.action_time = action_time;
        this.cooldown = cooldown;
        this.cost = cost;
        this.is_passive = is_passive;
    }

    public int getId() {
        return id_skill;
    }

    public void setId(Integer id) {
        this.id_skill = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public int getAction_time() {
        return action_time;
    }

    public void setAction_time(int action_time) {
        this.action_time = action_time;
    }

    public int getCooldown() {
        return cooldown;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public Boolean getIs_passive() {
        return is_passive;
    }

    public void setIs_passive(Boolean passive) {
        this.is_passive = passive;
    }

    public String get_owner() {
        if (CHARACTER != null) {
            String res = CHARACTER.getName();
            return res;
        } else {
            return "Owner not selected";
        }
    }
}
