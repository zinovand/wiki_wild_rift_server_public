package tjv.semestral_work.wiki_wild_rift.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Character {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_character;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private int health;

    @Column(nullable = false)
    private int move_speed;

    @Column(nullable = false)
    private double attack_speed;

    @Column(nullable = false)
    private int physical_damage;

    @Column(nullable = false)
    private int magic_damage;

    @Column(nullable = false)
    private int physical_armor;

    @Column(nullable = false)
    private int magic_armor;

    @ManyToOne
    private ClassCharacter CLASS;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "counterpicks", joinColumns = @JoinColumn(name = "id_source"), inverseJoinColumns = @JoinColumn(name = "id_target"))
    private Set<Character> STRONG_VS;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "STRONG_VS")
    private Set<Character> WEAK_VS;

    @OneToMany(mappedBy = "CHARACTER")
    private Set<Skill> SKILLS;

    public Character() {
    }

    public Character(String name, int health, int move_speed, double attack_speed, int physical_damage, int magic_damage, int physical_armor, int magic_armor) {
        this.name = name;
        this.health = health;
        this.move_speed = move_speed;
        this.attack_speed = attack_speed;
        this.physical_damage = physical_damage;
        this.magic_damage = magic_damage;
        this.physical_armor = physical_armor;
        this.magic_armor = magic_armor;
    }

    public int getId() {
        return id_character;
    }

    public void setId(Integer id) {
        this.id_character = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMove_speed() {
        return move_speed;
    }

    public void setMove_speed(int move_speed) {
        this.move_speed = move_speed;
    }

    public double getAttack_speed() {
        return attack_speed;
    }

    public void setAttack_speed(double attack_speed) {
        this.attack_speed = attack_speed;
    }

    public int getPhysical_damage() {
        return physical_damage;
    }

    public void setPhysical_damage(int physical_damage) {
        this.physical_damage = physical_damage;
    }

    public int getMagic_damage() {
        return magic_damage;
    }

    public void setMagic_damage(int magic_damage) {
        this.magic_damage = magic_damage;
    }

    public int getPhysical_armor() {
        return physical_armor;
    }

    public void setPhysical_armor(int physical_armor) {
        this.physical_armor = physical_armor;
    }

    public int getMagic_armor() {
        return magic_armor;
    }

    public void setMagic_armor(int magic_armor) {
        this.magic_armor = magic_armor;
    }

    public String get_class() {
        if (CLASS == null) {
            return "Not selected";
        } else {
            return CLASS.getName();
        }
    }

    public String get_skills() {
        String res_empty = "Not selected";
        String res = "";
        if (SKILLS != null) {
            int i = SKILLS.size();
            if (i == 0) {
                return res_empty;
            }
            for (Skill each : SKILLS) {
                if (i == 1) {
                    res += each.getName();
                } else {
                    res += each.getName() + ", ";
                }
                i--;
            }
        } else {
            return res_empty;
        }
        return res;
    }

    public String get_weak_vs() {
        String res_empty = "Not selected";
        String res = "";
        if (WEAK_VS != null) {
            int i = WEAK_VS.size();
            if (i == 0) {
                return res_empty;
            }
            for (Character each : WEAK_VS) {
                if (i == 1) {
                    res += each.getName();
                } else {
                    res += each.getName() + ", ";
                }
                i--;
            }
        } else {
            return res_empty;
        }
        return res;
    }

    public void setSTRONG_VS(Character character) {
        STRONG_VS.add(character);
    }

    public String get_strong_vs() {
        String res_empty = "Not selected";
        String res = "";
        if (STRONG_VS != null) {
            int i = STRONG_VS.size();
            if (i == 0) {
                return res_empty;
            }
            for (Character each : STRONG_VS) {
                if (i == 1) {
                    res += each.getName();
                } else {
                    res += each.getName() + ", ";
                }
                i--;
            }
        } else {
            return res_empty;
        }
        return res;
    }

    public void clear_counterpicks(Character character) {
        STRONG_VS.remove(character);
    }
}
