package tjv.semestral_work.wiki_wild_rift;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WikiWildRiftApplication {

    public static void main(String[] args) {
        SpringApplication.run(WikiWildRiftApplication.class, args);
    }

}
