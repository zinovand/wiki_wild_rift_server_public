package tjv.semestral_work.wiki_wild_rift.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import tjv.semestral_work.wiki_wild_rift.dto.SkillDTO;
import tjv.semestral_work.wiki_wild_rift.model.Skill;
import tjv.semestral_work.wiki_wild_rift.service.CharacterService;
import tjv.semestral_work.wiki_wild_rift.service.ClassService;
import tjv.semestral_work.wiki_wild_rift.service.SkillService;

import java.util.Set;


@RestController
public class SkillController {

    @Autowired
    private SkillService skillService;

    @Autowired
    private ClassService classService;

    @Autowired
    private CharacterService characterService;

    @GetMapping("/skill")
    public Set<SkillDTO> getAllSkills() {
        return skillService.getAll();
    }

    @GetMapping("/skill/{id}")
    public SkillDTO getOneSkill(@PathVariable Integer id) {
        if (!skillService.existId(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "ID not found");
        }
        return skillService.getById(id);
    }

    @PostMapping("/skill")
    @ResponseStatus(value = HttpStatus.CREATED)
    public SkillDTO addSkill(@RequestBody Skill newSkill) {
        if (newSkill.getName().length() == 0 ||
                newSkill.getEffect().length() == 0 ||
                newSkill.getIs_passive() == null ||
                (newSkill.getIs_passive() && (newSkill.getCooldown() != 0 || newSkill.getCost() != 0)) ||
                skillService.existName(newSkill.getName()) || skillService.existEffect(newSkill.getEffect())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect request body");
        }

        skillService.createSkill(newSkill);
        SkillDTO res;
        if ((res = skillService.getByName(newSkill.getName())) == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect request body");
        }
        return res;
    }

    @DeleteMapping("/skill/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteSkill(@PathVariable Integer id) {
        if (!skillService.existId(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "ID not found");
        }
        skillService.deleteById(id);
    }

    @PutMapping("/skill/{id}")
    public SkillDTO updateSkill(@PathVariable Integer id, @RequestBody Skill updSkill) {
        SkillDTO tmp_skill;
        if (skillService.existId(id)) {
            tmp_skill = skillService.getById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "id not found");
        }

        if (updSkill.getName().length() == 0 ||
                updSkill.getEffect().length() == 0 ||
                updSkill.getIs_passive() == null ||
                (updSkill.getIs_passive() && (updSkill.getCooldown() != 0 || updSkill.getCost() != 0)) ||
                (skillService.existName(updSkill.getName()) && !(updSkill.getName().equals(tmp_skill.getName()))) ||
                (skillService.existEffect(updSkill.getEffect()) && !(updSkill.getEffect().equals(tmp_skill.getEffect())))) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect request body");
        }
        skillService.updateById(id, updSkill);
        return skillService.getById(id);
    }

    @PutMapping("/skill/{id_skill}/character")
    public SkillDTO changeSkillOwner(@PathVariable Integer id_skill, @RequestParam Integer id_owner) {
        if (!skillService.existId(id_skill)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Skill ID not found");
        }
        if (!characterService.existId(id_owner)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Character ID not found");
        }
        return skillService.updSkillOwner(id_skill, id_owner);
    }

    @GetMapping("/skill/for_class")
    public Set<SkillDTO> getAllSkillForClass(@RequestParam String class_name) {
        if (!classService.existName(class_name)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Class not found");
        }
        return skillService.getAllSkillForClass(class_name);
    }

}
