package tjv.semestral_work.wiki_wild_rift.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import tjv.semestral_work.wiki_wild_rift.dto.ClassDTO;
import tjv.semestral_work.wiki_wild_rift.model.ClassCharacter;
import tjv.semestral_work.wiki_wild_rift.service.ClassService;

import java.util.Set;

@RestController
public class ClassController {

    @Autowired
    private ClassService classService;

    @GetMapping("/class")
    public Set<ClassDTO> getAllClasses() {
        return classService.getAll();
    }

    @GetMapping("/class/{id}")
    public ClassDTO getOneClass(@PathVariable Integer id) {
        if (!classService.existId(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "ID not found");
        }
        return classService.getById(id);
    }

    @PostMapping("/class")
    @ResponseStatus(value = HttpStatus.CREATED)
    //public ClassDTO addClass(@RequestBody ClassCharacter newClass){
    //public ResponseEntity<ClassDTO> addClass(@RequestBody ClassCharacter newClass){
    public ResponseEntity addClass(@RequestBody ClassCharacter newClass) {
        if (newClass.getName().length() == 0 || classService.existName(newClass.getName())) {
            //throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect request body");
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Incorrect request body");
        }
        classService.createClass(newClass);
        ClassDTO res;
        if ((res = classService.getByName(newClass.getName())) == null) {
            //throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect request body");
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Incorrect request body");
        }
        //return new ResponseEntity<ClassDTO>(res, HttpStatus.CREATED);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(res);
    }

    @DeleteMapping("/class/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteClass(@PathVariable Integer id) {
        if (!classService.existId(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "ID not found");
        }
        classService.deleteById(id);
    }

    @PutMapping("/class/{id}")
    public ClassDTO updateClass(@PathVariable Integer id, @RequestBody ClassCharacter updClass) {
        if (updClass.getName().length() == 0 || classService.existName(updClass.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect request body");
        }

        if (!classService.existId(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "ID not found");
        }

        classService.updateById(id, updClass);
        return classService.getById(id);
    }
}
