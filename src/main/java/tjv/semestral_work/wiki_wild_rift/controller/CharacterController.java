package tjv.semestral_work.wiki_wild_rift.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import tjv.semestral_work.wiki_wild_rift.dto.CharacterDTO;
import tjv.semestral_work.wiki_wild_rift.model.Character;
import tjv.semestral_work.wiki_wild_rift.service.CharacterService;
import tjv.semestral_work.wiki_wild_rift.service.ClassService;

import java.util.Set;

@RestController
public class CharacterController {

    @Autowired
    private CharacterService characterService;

    @Autowired
    private ClassService classService;

    @GetMapping("/character")
    public Set<CharacterDTO> getAllCharacters() {
        return characterService.getAll();
    }

    @GetMapping("/character/{id}")
    public CharacterDTO getOneCharacter(@PathVariable Integer id) {
        if (!characterService.existId(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "ID not found");
        }
        return characterService.getById(id);
    }

    @PostMapping("/character")
    @ResponseStatus(value = HttpStatus.CREATED)
    //public ResponseEntity<CharacterDTO> addCharacter(@RequestBody Character newCharacter){
    public CharacterDTO addCharacter(@RequestBody Character newCharacter) {
        if (newCharacter.getName().length() == 0 ||
                newCharacter.getHealth() < 1 ||
                newCharacter.getAttack_speed() < 0.1 ||
                newCharacter.getMove_speed() < 1 ||
                newCharacter.getPhysical_damage() < 1 ||
                newCharacter.getMagic_damage() < 1 ||
                newCharacter.getPhysical_armor() < 1 ||
                newCharacter.getMagic_armor() < 1 ||
                characterService.existName(newCharacter.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect request body");
        }

        characterService.createCharacter(newCharacter);
        CharacterDTO res;
        if ((res = characterService.getByName(newCharacter.getName())) == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect request body");
        }
        return res;
        //return new ResponseEntity<CharacterDTO>(characterService.postRequest(newCharacter), HttpStatus.CREATED);
    }

    @DeleteMapping("/character/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteCharacter(@PathVariable Integer id) {
        if (!characterService.existId(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "ID not found");
        }
        characterService.deleteById(id);
    }

    @PutMapping("/character/{id}")
    public CharacterDTO updateCharacter(@PathVariable Integer id, @RequestBody Character updCharacter) {
        String tmp_name;
        if (!characterService.existId(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "ID not found");
        } else {
            tmp_name = characterService.getById(id).getName();
        }
        if (updCharacter.getName().length() == 0 ||
                updCharacter.getHealth() < 1 ||
                updCharacter.getAttack_speed() < 0.1 ||
                updCharacter.getMove_speed() < 1 ||
                updCharacter.getPhysical_damage() < 0 ||
                updCharacter.getMagic_damage() < 0 ||
                updCharacter.getPhysical_armor() < 0 ||
                updCharacter.getMagic_armor() < 0
                || (characterService.existName(updCharacter.getName()) && !(updCharacter.getName().equals(tmp_name)))
        ) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect request body");
        }
        characterService.updateById(id, updCharacter);
        return characterService.getById(id);
    }

    @PutMapping("/character/{id_character}/class")
    public CharacterDTO changeClass(@PathVariable Integer id_character, @RequestParam Integer id_class) {
        if (!characterService.existId(id_character)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Character id not found");
        }

        if (!classService.existId(id_class)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Class id not found");
        }
        return characterService.updClassRequest(id_character, id_class);
    }

    @PutMapping("/counterpick")
    public CharacterDTO changeCounterpicks(@RequestParam Integer id_strong, @RequestParam Integer id_weak) {
        if (!characterService.existId(id_strong) || !characterService.existId(id_weak)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect request parameters");
        }
        return characterService.updCounterpicksRequest(id_strong, id_weak);
    }
}
