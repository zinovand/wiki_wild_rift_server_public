package tjv.semestral_work.wiki_wild_rift.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tjv.semestral_work.wiki_wild_rift.dto.Convertor;
import tjv.semestral_work.wiki_wild_rift.dto.SkillDTO;
import tjv.semestral_work.wiki_wild_rift.model.Skill;
import tjv.semestral_work.wiki_wild_rift.repository.SkillRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional(readOnly = true)
public class SkillService {
    Convertor conv = new Convertor();

    @Autowired
    private SkillRepository skillRepository;

    public Set<SkillDTO> getAll() {
        List<Skill> model_res = skillRepository.findAll();

        Set<SkillDTO> json_res = new LinkedHashSet<>();
        for (Skill each : model_res) {
            json_res.add(conv.toSkillDTO(each));
        }
        return json_res;
    }

    public SkillDTO getById(Integer id) {
        Skill model_res = skillRepository.getById(id);

        SkillDTO json_res = conv.toSkillDTO(model_res);
        return json_res;
    }

    public Boolean existId(Integer id) {
        return skillRepository.existsById(id);
    }

    public Boolean existName(String name) {
        return skillRepository.existsByName(name);
    }

    public Boolean existEffect(String effect) {
        return skillRepository.existsByEffect(effect);
    }

    public SkillDTO getByName(String name) {
        Skill model_res = skillRepository.getByName(name);
        SkillDTO json_res = conv.toSkillDTO(model_res);
        return json_res;
    }

    @Transactional
    public void createSkill(Skill skill) {
        skillRepository.save(skill);
    }

    @Transactional
    public void deleteById(Integer id) {
        skillRepository.deleteById(id);
    }

    @Transactional
    public void updateById(Integer id, Skill skill) {
        String name = skill.getName();
        String effect = skill.getEffect();
        Integer at = skill.getAction_time();
        Integer cost = skill.getCost();
        Integer cd = skill.getCooldown();
        Boolean passive = skill.getIs_passive();
        skillRepository.updateById(id, name, effect, at, cost, cd, passive);
    }

    @Transactional
    public SkillDTO updSkillOwner(Integer id_skill, Integer id_owner) {
        skillRepository.updateOwnerById(id_skill, id_owner);
        Skill model_res = skillRepository.getById(id_skill);
        SkillDTO json_res = conv.toSkillDTO(model_res);
        return json_res;
    }

    public Set<SkillDTO> getAllSkillForClass(String class_name) {
        List<Skill> model_res = skillRepository.getAllSkillForClass(class_name);

        Set<SkillDTO> json_res = new LinkedHashSet<>();
        for (Skill each : model_res) {
            json_res.add(conv.toSkillDTO(each));
        }
        return json_res;
    }

}
