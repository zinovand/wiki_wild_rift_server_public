package tjv.semestral_work.wiki_wild_rift.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tjv.semestral_work.wiki_wild_rift.dto.ClassDTO;
import tjv.semestral_work.wiki_wild_rift.dto.Convertor;
import tjv.semestral_work.wiki_wild_rift.model.ClassCharacter;
import tjv.semestral_work.wiki_wild_rift.repository.ClassRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional(readOnly = true)
public class ClassService {
    Convertor conv = new Convertor();

    @Autowired
    private ClassRepository classRepository;

    public Set<ClassDTO> getAll() {
        List<ClassCharacter> model_res = classRepository.findAll();

        Set<ClassDTO> json_res = new LinkedHashSet<>();
        for (ClassCharacter each : model_res) {
            json_res.add(conv.toClassDTO(each));
        }
        return json_res;
    }

    public ClassDTO getById(Integer id) {
        ClassCharacter model_res = classRepository.getById(id);

        ClassDTO json_res = conv.toClassDTO(model_res);
        return json_res;
    }

    public Boolean existId(Integer id) {
        return classRepository.existsById(id);
    }

    public Boolean existName(String name) {
        return classRepository.existsByName(name);
    }

    public ClassDTO getByName(String name) {
        ClassCharacter model_res = classRepository.getByName(name);
        ClassDTO json_res = conv.toClassDTO(model_res);
        return json_res;
    }

    @Transactional
    public void createClass(ClassCharacter classCharacter) {
        classRepository.save(classCharacter);
    }

    @Transactional
    public void deleteById(Integer id) {
        classRepository.deleteLinks(id);
        classRepository.deleteById(id);
    }

    @Transactional
    public void updateById(Integer id, ClassCharacter classCharacter) {
        classRepository.updateById(id, classCharacter.getName());
    }
}
