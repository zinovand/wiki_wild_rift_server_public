package tjv.semestral_work.wiki_wild_rift.service;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tjv.semestral_work.wiki_wild_rift.dto.CharacterDTO;
import tjv.semestral_work.wiki_wild_rift.dto.ClassDTO;
import tjv.semestral_work.wiki_wild_rift.dto.Convertor;
import tjv.semestral_work.wiki_wild_rift.model.Character;
import tjv.semestral_work.wiki_wild_rift.model.ClassCharacter;
import tjv.semestral_work.wiki_wild_rift.repository.CharacterRepository;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional(readOnly = true)
public class CharacterService {
    Convertor conv = new Convertor();

    @Autowired
    private CharacterRepository characterRepository;

    public Set<CharacterDTO> getAll() {
        List<Character> model_res = characterRepository.findAll();

        Set<CharacterDTO> json_res = new LinkedHashSet<>();
        for (Character each : model_res) {
            json_res.add(conv.toCharacterDTO(each));
        }
        return json_res;
    }

    public CharacterDTO getById(Integer id) {
        Character model_res = characterRepository.getById(id);
        CharacterDTO json_res = conv.toCharacterDTO(model_res);
        return json_res;
    }

    public Boolean existId(Integer id) {
        return characterRepository.existsById(id);
    }

    public Boolean existName(String name) {
        return characterRepository.existsByName(name);
    }

    public CharacterDTO getByName(String name) {
        Character model_res = characterRepository.getByName(name);
        CharacterDTO json_res = conv.toCharacterDTO(model_res);
        return json_res;
    }

    @Transactional
    public CharacterDTO updClassRequest(Integer id_character, Integer id_class) {
        characterRepository.updateClassById(id_character, id_class);
        Character model_res = characterRepository.getById(id_character);
        CharacterDTO json_res = conv.toCharacterDTO(model_res);
        return json_res;
    }

    @Transactional
    public CharacterDTO updCounterpicksRequest(Integer id_strong, Integer id_weak) {
        Character model_res = characterRepository.getById(id_strong);
        model_res.setSTRONG_VS(characterRepository.getById(id_weak));
        //model_res = characterRepository.getById(id_strong);
        CharacterDTO json_res = conv.toCharacterDTO(model_res);
        return json_res;
    }

    @Transactional
    public void createCharacter(Character character) {
        characterRepository.save(character);
    }

    @Transactional
    public void deleteById(Integer id) {
        characterRepository.deleteLinksSkill(id);
        List<Character> res_all = characterRepository.findAll();
        //Character del_res = characterRepository.findById(id).get();
        Character del_res = characterRepository.getById(id);
        for (Character each : res_all) {
            each.clear_counterpicks(del_res);
        }
        characterRepository.deleteById(id);
    }

    @Transactional
    public void updateById(Integer id, Character character) {
        String name = character.getName();
        Integer hp = character.getHealth();
        Integer ms = character.getMove_speed();
        Double as = character.getAttack_speed();
        Integer pa = character.getPhysical_armor();
        Integer pd = character.getPhysical_damage();
        Integer ma = character.getMagic_armor();
        Integer md = character.getMagic_damage();
        characterRepository.updateById(id, name, hp, ms, as, pd, md, pa, ma);
    }
}
