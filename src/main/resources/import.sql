INSERT INTO class_character VALUES (DEFAULT, 'Knight');
INSERT INTO class_character VALUES (DEFAULT, 'Archer');
INSERT INTO class_character VALUES (DEFAULT, 'Mage');

INSERT INTO character (id_character,class_id_class,name,health, move_speed, attack_speed, physical_damage, magic_damage, physical_armor, magic_armor) VALUES (DEFAULT, 3, 'Orianna', 702, 400, 1.17, 73, 66, 47, 39);
INSERT INTO character (id_character,class_id_class, name,health, move_speed, attack_speed, physical_damage, magic_damage, physical_armor, magic_armor) VALUES (DEFAULT, 3, 'Kennen', 460, 328, 1.3, 78, 88, 39, 33);
INSERT INTO character (id_character,class_id_class, name,health, move_speed, attack_speed, physical_damage, magic_damage, physical_armor, magic_armor) VALUES (DEFAULT, 2, 'Nasus', 437, 334, 1.27, 73, 95, 50, 23);
INSERT INTO character (id_character,class_id_class, name,health, move_speed, attack_speed, physical_damage, magic_damage, physical_armor, magic_armor) VALUES (DEFAULT, 1, 'Yasuo', 404, 314, 1.46, 88, 84, 39, 17);

INSERT INTO skill(id_skill,character_id_character, name, effect, action_time, cooldown, cost, is_passive) VALUES (DEFAULT, 1, 'Act skill id1', 'EFFECT1', 10, 120, 0, false);
INSERT INTO skill(id_skill,character_id_character, name, effect, action_time, cooldown, cost, is_passive) VALUES (DEFAULT, 1, 'Pas skill id2', 'EFFECT2', 0, 0, 0, true);
INSERT INTO skill(id_skill,character_id_character, name, effect, action_time, cooldown, cost, is_passive) VALUES (DEFAULT, 4, 'Pas skill id3', 'EFFECT3', 0, 0, 0, true);
INSERT INTO skill(id_skill,character_id_character, name, effect, action_time, cooldown, cost, is_passive) VALUES (DEFAULT, 4, 'Act skill id4', 'EFFECT4', 10, 120, 0, false);

INSERT INTO counterpicks(id_source, id_target) VALUES (1,2);
INSERT INTO counterpicks(id_source, id_target) VALUES (1,4);
INSERT INTO counterpicks(id_source, id_target) VALUES (3,4);
INSERT INTO counterpicks(id_source, id_target) VALUES (4,2);