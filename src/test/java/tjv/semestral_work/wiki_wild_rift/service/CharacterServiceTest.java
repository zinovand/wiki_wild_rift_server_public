package tjv.semestral_work.wiki_wild_rift.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import tjv.semestral_work.wiki_wild_rift.dto.CharacterDTO;
import tjv.semestral_work.wiki_wild_rift.dto.ClassDTO;
import tjv.semestral_work.wiki_wild_rift.dto.Convertor;
import tjv.semestral_work.wiki_wild_rift.model.Character;
import tjv.semestral_work.wiki_wild_rift.model.ClassCharacter;
import tjv.semestral_work.wiki_wild_rift.repository.CharacterRepository;
import tjv.semestral_work.wiki_wild_rift.repository.ClassRepository;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

//ALL TESTS TEST POSITIVE SCENARIOS ONLY, BECAUSE SERVICES NOT VALIDATE DATA (CONTROLLERS DO THIS)
//@SpringBootTest
@ExtendWith(MockitoExtension.class)
class CharacterServiceTest {

    //@Autowired
    @InjectMocks
    CharacterService characterService;

    //@MockBean
    @Mock
    CharacterRepository characterRepository;

    Integer EXISTS_ID = 555;
    Integer NOT_EXISTS_ID = 123;
    Character character_test_1 = new Character("CHARACTER_TEST_1", 112, 113, 1.14, 15, 16, 17, 18);

    Convertor conv = new Convertor();

    @BeforeEach
    void setId() {
        character_test_1.setId(EXISTS_ID);
    }

    @Test
    void getAll() {
        Character character_test_2 = new Character("CHARACTER_TEST_2", 222, 223, 2.24, 25, 26, 27, 28);
        character_test_2.setId(EXISTS_ID + 1);
        List<Character> allCharacters = List.of(character_test_1, character_test_2);

        Mockito.when(characterRepository.findAll()).thenReturn(allCharacters);

        Set<CharacterDTO> res = characterService.getAll();
        boolean character_test_1_ok = false;
        boolean character_test_2_ok = false;

        assertEquals(2, res.size());
        for (CharacterDTO each : res) {
            if (each.getName() == character_test_1.getName() &&
                    each.getId() == character_test_1.getId() &&
                    each.getHealth() == character_test_1.getHealth() &&
                    each.getMove_speed() == character_test_1.getMove_speed() &&
                    each.getAttack_speed() == character_test_1.getAttack_speed() &&
                    each.getPhysical_damage() == character_test_1.getPhysical_damage() &&
                    each.getMagic_damage() == character_test_1.getMagic_damage() &&
                    each.getPhysical_armor() == character_test_1.getPhysical_armor() &&
                    each.getMagic_armor() == character_test_1.getMagic_armor()) {
                character_test_1_ok = true;
            }
            if (each.getName() == character_test_2.getName() &&
                    each.getId() == character_test_2.getId() &&
                    each.getHealth() == character_test_2.getHealth() &&
                    each.getMove_speed() == character_test_2.getMove_speed() &&
                    each.getAttack_speed() == character_test_2.getAttack_speed() &&
                    each.getPhysical_damage() == character_test_2.getPhysical_damage() &&
                    each.getMagic_damage() == character_test_2.getMagic_damage() &&
                    each.getPhysical_armor() == character_test_2.getPhysical_armor() &&
                    each.getMagic_armor() == character_test_2.getMagic_armor()) {
                character_test_2_ok = true;
            }
        }
        assertTrue(character_test_1_ok);
        assertTrue(character_test_2_ok);
    }

    @Test
    void getById() {
        Mockito.when(characterRepository.getById(EXISTS_ID)).thenReturn(character_test_1);

        CharacterDTO res = characterService.getById(EXISTS_ID);
        CharacterDTO ref = conv.toCharacterDTO(character_test_1);
        assertEquals(res.getId(), ref.getId());
        assertEquals(res.getName(), ref.getName());
        assertEquals(res.getHealth(), ref.getHealth());
        assertEquals(res.getMove_speed(), ref.getMove_speed());
        assertEquals(res.getAttack_speed(), ref.getAttack_speed());
        assertEquals(res.getPhysical_damage(), ref.getPhysical_damage());
        assertEquals(res.getMagic_damage(), ref.getMagic_damage());
        assertEquals(res.getMagic_armor(), ref.getMagic_armor());
        assertEquals(res.getPhysical_armor(), ref.getPhysical_armor());
    }

    @Test
    void getByName() {
        Mockito.when(characterRepository.getByName(character_test_1.getName())).thenReturn(character_test_1);

        CharacterDTO res = characterService.getByName(character_test_1.getName());
        CharacterDTO ref = conv.toCharacterDTO(character_test_1);
        assertEquals(res.getId(), ref.getId());
        assertEquals(res.getName(), ref.getName());
        assertEquals(res.getHealth(), ref.getHealth());
        assertEquals(res.getMove_speed(), ref.getMove_speed());
        assertEquals(res.getAttack_speed(), ref.getAttack_speed());
        assertEquals(res.getPhysical_damage(), ref.getPhysical_damage());
        assertEquals(res.getMagic_damage(), ref.getMagic_damage());
        assertEquals(res.getMagic_armor(), ref.getMagic_armor());
        assertEquals(res.getPhysical_armor(), ref.getPhysical_armor());
    }

    @Test
    void existId() {
        Mockito.when(characterRepository.existsById(EXISTS_ID)).thenReturn(true);
        assertTrue(characterService.existId(EXISTS_ID));

        Mockito.when(characterRepository.existsById(NOT_EXISTS_ID)).thenReturn(false);
        assertFalse(characterService.existId(NOT_EXISTS_ID));
    }

    @Test
    void existName() {
        Mockito.when(characterRepository.existsByName(character_test_1.getName())).thenReturn(true);
        assertTrue(characterService.existName(character_test_1.getName()));

        Mockito.when(characterRepository.existsByName(character_test_1.getName())).thenReturn(false);
        assertFalse(characterService.existName(character_test_1.getName()));
    }

    @Test
    void createCharacter() {
        characterService.createCharacter(character_test_1);
        ArgumentCaptor<Character> argumentCaptor = ArgumentCaptor.forClass(Character.class);
        Mockito.verify(characterRepository, Mockito.times(1)).save(argumentCaptor.capture());
        Character dataProvidedToService = argumentCaptor.getValue();
        assertEquals(character_test_1.getName(), dataProvidedToService.getName());
        assertEquals(character_test_1.getHealth(), dataProvidedToService.getHealth());
        assertEquals(character_test_1.getMove_speed(), dataProvidedToService.getMove_speed());
        assertEquals(character_test_1.getAttack_speed(), dataProvidedToService.getAttack_speed());
        assertEquals(character_test_1.getPhysical_damage(), dataProvidedToService.getPhysical_damage());
        assertEquals(character_test_1.getMagic_damage(), dataProvidedToService.getMagic_damage());
        assertEquals(character_test_1.getPhysical_armor(), dataProvidedToService.getPhysical_armor());
        assertEquals(character_test_1.getMagic_armor(), dataProvidedToService.getMagic_armor());
    }

    @Test
    void deleteById() {
//        Mockito.when(characterRepository.getById(EXISTS_ID)).thenReturn(character_test_1);
//        Character character_test_2 = new Character("CHARACTER_TEST_2", 222, 223, 2.24, 25, 26, 27, 28);
//        character_test_2.setId(EXISTS_ID+1);
//        List<Character> allCharacters = List.of(character_test_1, character_test_2);
//
//        Mockito.when(characterRepository.findAll()).thenReturn(allCharacters);

        characterService.deleteById(EXISTS_ID);
        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);

        Mockito.verify(characterRepository, Mockito.times(1)).deleteLinksSkill(idCaptor.capture());
        assertEquals(EXISTS_ID, idCaptor.getValue());

        Mockito.verify(characterRepository, Mockito.times(1)).deleteById(idCaptor.capture());
        assertEquals(EXISTS_ID, idCaptor.getValue());
    }

    @Test
    void updateById() {
        characterService.updateById(EXISTS_ID, character_test_1);
        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> hpCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> msCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Double> asCaptor = ArgumentCaptor.forClass(Double.class);
        ArgumentCaptor<Integer> pdCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> mdCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> paCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> maCaptor = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(characterRepository, Mockito.times(1)).updateById(idCaptor.capture(),
                nameCaptor.capture(),
                hpCaptor.capture(),
                msCaptor.capture(),
                asCaptor.capture(),
                pdCaptor.capture(),
                mdCaptor.capture(),
                paCaptor.capture(),
                maCaptor.capture());
        assertEquals(character_test_1.getName(), nameCaptor.getValue());
        assertEquals(character_test_1.getHealth(), hpCaptor.getValue());
        assertEquals(character_test_1.getMove_speed(), msCaptor.getValue());
        assertEquals(character_test_1.getAttack_speed(), asCaptor.getValue());
        assertEquals(character_test_1.getPhysical_damage(), pdCaptor.getValue());
        assertEquals(character_test_1.getMagic_damage(), mdCaptor.getValue());
        assertEquals(character_test_1.getPhysical_armor(), paCaptor.getValue());
        assertEquals(character_test_1.getMagic_armor(), maCaptor.getValue());
        assertEquals(EXISTS_ID, idCaptor.getValue());
    }

    @Test
    void updClassRequest() {
        Integer id_class = 777;
        Mockito.when(characterRepository.getById(EXISTS_ID)).thenReturn(character_test_1);

        characterService.updClassRequest(EXISTS_ID, id_class);
        ArgumentCaptor<Integer> idCharacterCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> idClassCaptor = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(characterRepository, Mockito.times(1)).updateClassById(idCharacterCaptor.capture(), idClassCaptor.capture());
        assertEquals(EXISTS_ID, idCharacterCaptor.getValue());
        assertEquals(id_class, idClassCaptor.getValue());
    }


}