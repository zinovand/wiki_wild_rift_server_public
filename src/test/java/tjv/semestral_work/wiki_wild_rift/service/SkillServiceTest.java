package tjv.semestral_work.wiki_wild_rift.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import tjv.semestral_work.wiki_wild_rift.dto.CharacterDTO;
import tjv.semestral_work.wiki_wild_rift.dto.Convertor;
import tjv.semestral_work.wiki_wild_rift.dto.SkillDTO;
import tjv.semestral_work.wiki_wild_rift.model.Character;
import tjv.semestral_work.wiki_wild_rift.model.Skill;
import tjv.semestral_work.wiki_wild_rift.repository.CharacterRepository;
import tjv.semestral_work.wiki_wild_rift.repository.SkillRepository;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

//ALL TESTS TEST POSITIVE SCENARIOS ONLY, BECAUSE SERVICES NOT VALIDATE DATA (CONTROLLERS DO THIS)
//@SpringBootTest
@ExtendWith(MockitoExtension.class)
class SkillServiceTest {

    //@Autowired
    @InjectMocks
    SkillService skillService;

    //@MockBean
    @Mock
    SkillRepository skillRepository;

    Integer EXISTS_ID = 555;
    Integer NOT_EXISTS_ID = 123;
    Skill skill_test_1 = new Skill("SKILL_TEST_1", "EFFECT_TEST_1", 12, 13, 14, false);
    Convertor conv = new Convertor();

    @BeforeEach
    void setId() {
        skill_test_1.setId(EXISTS_ID);
    }

    @Test
    void getAll() {
        Skill skill_test_2 = new Skill("SKILL_TEST_2", "EFFECT_TEST_2", 22, 0, 0, true);
        skill_test_2.setId(EXISTS_ID + 1);
        List<Skill> allSkills = List.of(skill_test_1, skill_test_2);

        Mockito.when(skillRepository.findAll()).thenReturn(allSkills);

        Set<SkillDTO> res = skillService.getAll();
        boolean skill_test_1_ok = false;
        boolean skill_test_2_ok = false;

        assertEquals(2, res.size());
        for (SkillDTO each : res) {
            if (each.getName() == skill_test_1.getName() &&
                    each.getId() == skill_test_1.getId() &&
                    each.getEffect() == skill_test_1.getEffect() &&
                    each.getCooldown() == skill_test_1.getCooldown() &&
                    each.getAction_time() == skill_test_1.getAction_time() &&
                    each.getCost() == skill_test_1.getCost() &&
                    each.getIs_passive() == skill_test_1.getIs_passive()) {
                skill_test_1_ok = true;
            }
            if (each.getName() == skill_test_2.getName() &&
                    each.getId() == skill_test_2.getId() &&
                    each.getEffect() == skill_test_2.getEffect() &&
                    each.getCooldown() == skill_test_2.getCooldown() &&
                    each.getAction_time() == skill_test_2.getAction_time() &&
                    each.getCost() == skill_test_2.getCost() &&
                    each.getIs_passive() == skill_test_2.getIs_passive()) {
                skill_test_2_ok = true;
            }
        }
        assertTrue(skill_test_1_ok);
        assertTrue(skill_test_2_ok);
    }

    @Test
    void getById() {
        Mockito.when(skillRepository.getById(EXISTS_ID)).thenReturn(skill_test_1);

        SkillDTO res = skillService.getById(EXISTS_ID);
        SkillDTO ref = conv.toSkillDTO(skill_test_1);
        assertEquals(res.getId(), ref.getId());
        assertEquals(res.getName(), ref.getName());
        assertEquals(res.getEffect(), ref.getEffect());
        assertEquals(res.getAction_time(), ref.getAction_time());
        assertEquals(res.getCooldown(), ref.getCooldown());
        assertEquals(res.getCost(), ref.getCost());
        assertEquals(res.getIs_passive(), ref.getIs_passive());
    }

    @Test
    void getByName() {
        Mockito.when(skillRepository.getByName(skill_test_1.getName())).thenReturn(skill_test_1);

        SkillDTO res = skillService.getByName(skill_test_1.getName());
        SkillDTO ref = conv.toSkillDTO(skill_test_1);
        assertEquals(res.getId(), ref.getId());
        assertEquals(res.getName(), ref.getName());
        assertEquals(res.getEffect(), ref.getEffect());
        assertEquals(res.getAction_time(), ref.getAction_time());
        assertEquals(res.getCooldown(), ref.getCooldown());
        assertEquals(res.getCost(), ref.getCost());
        assertEquals(res.getIs_passive(), ref.getIs_passive());
    }


    @Test
    void existId() {
        Mockito.when(skillRepository.existsById(EXISTS_ID)).thenReturn(true);
        assertTrue(skillService.existId(EXISTS_ID));

        Mockito.when(skillRepository.existsById(NOT_EXISTS_ID)).thenReturn(false);
        assertFalse(skillService.existId(NOT_EXISTS_ID));
    }

    @Test
    void existName() {
        Mockito.when(skillRepository.existsByName(skill_test_1.getName())).thenReturn(true);
        assertTrue(skillService.existName(skill_test_1.getName()));

        Mockito.when(skillRepository.existsByName(skill_test_1.getName())).thenReturn(false);
        assertFalse(skillService.existName(skill_test_1.getName()));
    }

    @Test
    void existEffect() {
        Mockito.when(skillRepository.existsByEffect(skill_test_1.getEffect())).thenReturn(true);
        assertTrue(skillService.existEffect(skill_test_1.getEffect()));

        Mockito.when(skillRepository.existsByEffect(skill_test_1.getEffect())).thenReturn(false);
        assertFalse(skillService.existEffect(skill_test_1.getEffect()));
    }


    @Test
    void createSkill() {
        skillService.createSkill(skill_test_1);
        ArgumentCaptor<Skill> argumentCaptor = ArgumentCaptor.forClass(Skill.class);
        Mockito.verify(skillRepository, Mockito.times(1)).save(argumentCaptor.capture());
        Skill dataProvidedToService = argumentCaptor.getValue();
        assertEquals(skill_test_1.getName(), dataProvidedToService.getName());
        assertEquals(skill_test_1.getEffect(), dataProvidedToService.getEffect());
        assertEquals(skill_test_1.getAction_time(), dataProvidedToService.getAction_time());
        assertEquals(skill_test_1.getCooldown(), dataProvidedToService.getCooldown());
        assertEquals(skill_test_1.getCost(), dataProvidedToService.getCost());
        assertEquals(skill_test_1.getIs_passive(), dataProvidedToService.getIs_passive());
    }

    @Test
    void deleteById() {
        skillService.deleteById(EXISTS_ID);
        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);

        Mockito.verify(skillRepository, Mockito.times(1)).deleteById(idCaptor.capture());
        assertEquals(EXISTS_ID, idCaptor.getValue());
    }

    @Test
    void updateById() {
        skillService.updateById(EXISTS_ID, skill_test_1);
        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> effectCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> action_timeCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> cdCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> costCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Boolean> is_passiveCaptor = ArgumentCaptor.forClass(Boolean.class);
        Mockito.verify(skillRepository, Mockito.times(1)).updateById(idCaptor.capture(),
                nameCaptor.capture(),
                effectCaptor.capture(),
                action_timeCaptor.capture(),
                costCaptor.capture(),
                cdCaptor.capture(),
                is_passiveCaptor.capture());
        assertEquals(skill_test_1.getName(), nameCaptor.getValue());
        assertEquals(skill_test_1.getEffect(), effectCaptor.getValue());
        assertEquals(skill_test_1.getAction_time(), action_timeCaptor.getValue());
        assertEquals(skill_test_1.getCost(), costCaptor.getValue());
        assertEquals(skill_test_1.getCooldown(), cdCaptor.getValue());
        assertEquals(skill_test_1.getIs_passive(), is_passiveCaptor.getValue());
        assertEquals(EXISTS_ID, idCaptor.getValue());
    }

    @Test
    void updSkillOwner() {
        Integer id_owner = 777;
        Mockito.when(skillRepository.getById(EXISTS_ID)).thenReturn(skill_test_1);

        skillService.updSkillOwner(EXISTS_ID, id_owner);
        ArgumentCaptor<Integer> idSkillCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> idOwnerCaptor = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(skillRepository, Mockito.times(1)).updateOwnerById(idSkillCaptor.capture(), idOwnerCaptor.capture());
        assertEquals(EXISTS_ID, idSkillCaptor.getValue());
        assertEquals(id_owner, idOwnerCaptor.getValue());
    }

    @Test
    void getAllSkillForClass() {
        String class_name_test = "CLASS_NAME_TEST";
        Skill skill_test_2 = new Skill("SKILL_TEST_2", "EFFECT_TEST_2", 22, 0, 0, true);
        skill_test_2.setId(EXISTS_ID + 1);
        List<Skill> allSkillsForClass = List.of(skill_test_1, skill_test_2);

        Mockito.when(skillRepository.getAllSkillForClass(class_name_test)).thenReturn(allSkillsForClass);

        Set<SkillDTO> res = skillService.getAllSkillForClass(class_name_test);
        boolean skill_test_1_ok = false;
        boolean skill_test_2_ok = false;

        assertEquals(2, res.size());
        for (SkillDTO each : res) {
            if (each.getName() == skill_test_1.getName() &&
                    each.getId() == skill_test_1.getId() &&
                    each.getEffect() == skill_test_1.getEffect() &&
                    each.getCooldown() == skill_test_1.getCooldown() &&
                    each.getAction_time() == skill_test_1.getAction_time() &&
                    each.getCost() == skill_test_1.getCost() &&
                    each.getIs_passive() == skill_test_1.getIs_passive()) {
                skill_test_1_ok = true;
            }
            if (each.getName() == skill_test_2.getName() &&
                    each.getId() == skill_test_2.getId() &&
                    each.getEffect() == skill_test_2.getEffect() &&
                    each.getCooldown() == skill_test_2.getCooldown() &&
                    each.getAction_time() == skill_test_2.getAction_time() &&
                    each.getCost() == skill_test_2.getCost() &&
                    each.getIs_passive() == skill_test_2.getIs_passive()) {
                skill_test_2_ok = true;
            }
        }
        assertTrue(skill_test_1_ok);
        assertTrue(skill_test_2_ok);
    }
}