package tjv.semestral_work.wiki_wild_rift.service;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import tjv.semestral_work.wiki_wild_rift.dto.ClassDTO;
import tjv.semestral_work.wiki_wild_rift.dto.Convertor;
import tjv.semestral_work.wiki_wild_rift.model.ClassCharacter;
import tjv.semestral_work.wiki_wild_rift.repository.ClassRepository;

import javax.persistence.criteria.CriteriaBuilder;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

//ALL TESTS TEST POSITIVE SCENARIOS ONLY, BECAUSE SERVICES NOT VALIDATE DATA (CONTROLLERS DO THIS)
//@SpringBootTest
@ExtendWith(MockitoExtension.class)
class ClassServiceTest {

    //@Autowired
    @InjectMocks
    ClassService classService;

    //@MockBean
    @Mock
    ClassRepository classRepository;

    Integer EXISTS_ID = 555;
    Integer NOT_EXISTS_ID = 123;
    ClassCharacter class_test_1 = new ClassCharacter("CLASS_TEST_1");

    Convertor conv = new Convertor();

    @BeforeEach
    void setId() {
        class_test_1.setId(EXISTS_ID);
    }

    @Test
    void getAll() {
        ClassCharacter class_test_2 = new ClassCharacter("CLASS_TEST_2");
        class_test_2.setId(EXISTS_ID + 1);
        List<ClassCharacter> allClasses = List.of(class_test_1, class_test_2);

        Mockito.when(classRepository.findAll()).thenReturn(allClasses);

        Set<ClassDTO> res = classService.getAll();
        boolean class_test_1_ok = false;
        boolean class_test_2_ok = false;

        assertEquals(2, res.size());
        for (ClassDTO each : res) {
            if (each.getName() == class_test_1.getName() && each.getId() == class_test_1.getId()) {
                class_test_1_ok = true;
            }
            if (each.getName() == class_test_2.getName() && each.getId() == class_test_2.getId()) {
                class_test_2_ok = true;
            }
        }
        assertTrue(class_test_1_ok);
        assertTrue(class_test_2_ok);
    }


    @Test
    void getById() {
        Mockito.when(classRepository.getById(EXISTS_ID)).thenReturn(class_test_1);

        ClassDTO res = classService.getById(EXISTS_ID);
        assertEquals(res.getId(), conv.toClassDTO(class_test_1).getId());
        assertEquals(res.getName(), conv.toClassDTO(class_test_1).getName());
    }

    @Test
    void getByName() {
        Mockito.when(classRepository.getByName(class_test_1.getName())).thenReturn(class_test_1);

        ClassDTO res = classService.getByName(class_test_1.getName());
        assertEquals(res.getId(), conv.toClassDTO(class_test_1).getId());
        assertEquals(res.getName(), conv.toClassDTO(class_test_1).getName());
    }

    @Test
    void existId() {
        Mockito.when(classRepository.existsById(EXISTS_ID)).thenReturn(true);
        assertTrue(classService.existId(EXISTS_ID));

        Mockito.when(classRepository.existsById(NOT_EXISTS_ID)).thenReturn(false);
        assertFalse(classService.existId(NOT_EXISTS_ID));
    }

    @Test
    void existName() {
        Mockito.when(classRepository.existsByName(class_test_1.getName())).thenReturn(true);
        assertTrue(classService.existName(class_test_1.getName()));

        Mockito.when(classRepository.existsByName(class_test_1.getName())).thenReturn(false);
        assertFalse(classService.existName(class_test_1.getName()));
    }

    @Test
    void createClass() {
        classService.createClass(class_test_1);
        ArgumentCaptor<ClassCharacter> argumentCaptor = ArgumentCaptor.forClass(ClassCharacter.class);
        Mockito.verify(classRepository, Mockito.times(1)).save(argumentCaptor.capture());
        ClassCharacter dataProvidedToService = argumentCaptor.getValue();
        assertEquals(class_test_1.getName(), dataProvidedToService.getName());
    }

    @Test
    void deleteById() {
        classService.deleteById(EXISTS_ID);
        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);

        Mockito.verify(classRepository, Mockito.times(1)).deleteLinks(idCaptor.capture());
        assertEquals(EXISTS_ID, idCaptor.getValue());

        Mockito.verify(classRepository, Mockito.times(1)).deleteById(idCaptor.capture());
        assertEquals(EXISTS_ID, idCaptor.getValue());
    }

    @Test
    void updateById() {
        classService.updateById(EXISTS_ID, class_test_1);
        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(classRepository, Mockito.times(1)).updateById(idCaptor.capture(), argumentCaptor.capture());
        assertEquals(class_test_1.getName(), argumentCaptor.getValue());
        assertEquals(EXISTS_ID, idCaptor.getValue());
    }
}