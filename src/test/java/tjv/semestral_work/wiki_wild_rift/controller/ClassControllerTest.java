package tjv.semestral_work.wiki_wild_rift.controller;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import tjv.semestral_work.wiki_wild_rift.dto.ClassDTO;
import tjv.semestral_work.wiki_wild_rift.dto.Convertor;
import tjv.semestral_work.wiki_wild_rift.model.ClassCharacter;
import tjv.semestral_work.wiki_wild_rift.service.ClassService;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ClassController.class)
class ClassControllerTest {

    //SET
    @MockBean
    ClassService classService;

    @Autowired
    MockMvc mockMvc;

    ClassCharacter class_test_1 = new ClassCharacter("TEST_CLASS_1");
    Integer EXISTS_ID = 555;
    Integer NOT_EXISTS_ID = 123;
    String ALREADY_EXISTS_NAME = "ALREADY_EXISTS_NAME";
    String UPDATED_NAME = "UPDATED_NAME";
    Convertor conv = new Convertor();

    @BeforeEach
    void setId() {
        class_test_1.setId(EXISTS_ID);
        Mockito.when(classService.existId(EXISTS_ID)).thenReturn(true);
        Mockito.when(classService.existId(NOT_EXISTS_ID)).thenReturn(false);
        Mockito.when(classService.existName(ALREADY_EXISTS_NAME)).thenReturn(true);
    }
    ///

    //GET ALL
    @Test
    void getAllClasses() throws Exception {
        ClassCharacter class_test_2 = new ClassCharacter("CLASS_TEST_2");
        class_test_2.setId(EXISTS_ID + 1);
        Set<ClassDTO> allClasses = new LinkedHashSet<>();
        allClasses.add(conv.toClassDTO(class_test_1));
        allClasses.add(conv.toClassDTO(class_test_2));

        Mockito.when(classService.getAll()).thenReturn(allClasses);

        mockMvc.perform(MockMvcRequestBuilders.get("/class")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(allClasses.size())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is(class_test_1.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(class_test_1.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", Matchers.is(class_test_2.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", Matchers.is(class_test_2.getId())));
    }
    ///


    //GET ONE
    @Test
    void getOneClassPositive() throws Exception {
        Mockito.when(classService.getById(EXISTS_ID)).thenReturn(conv.toClassDTO(class_test_1));
        mockMvc.perform(MockMvcRequestBuilders.get("/class/" + EXISTS_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is(class_test_1.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(class_test_1.getId())));
    }

    @Test
    void getOneClassNegativeNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/class/" + NOT_EXISTS_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        Mockito.verify(classService, Mockito.never()).getById(NOT_EXISTS_ID);
    }
    ///

    //ADD NEW
    @Test
    void addClassPositive() throws Exception {
        Mockito.when(classService.existName(class_test_1.getName())).thenReturn(false);
        Mockito.when(classService.getByName(class_test_1.getName())).thenReturn(conv.toClassDTO(class_test_1));

        mockMvc.perform(post("/class")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + class_test_1.getName() + "\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", Matchers.is(class_test_1.getName())));

        ArgumentCaptor<ClassCharacter> argumentCaptor = ArgumentCaptor.forClass(ClassCharacter.class);
        Mockito.verify(classService, Mockito.times(1)).createClass(argumentCaptor.capture());
        ClassCharacter dataProvidedToService = argumentCaptor.getValue();
        assertEquals(class_test_1.getName(), dataProvidedToService.getName());
    }

    @Test
    void addClassNegativeAlreadyExists() throws Exception {
        mockMvc.perform(post("/class")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + ALREADY_EXISTS_NAME + "\"}"))
                .andExpect(status().isBadRequest());

        ArgumentCaptor<ClassCharacter> argumentCaptor = ArgumentCaptor.forClass(ClassCharacter.class);
        Mockito.verify(classService, Mockito.never()).createClass(argumentCaptor.capture());
    }

    @Test
    void addClassNegativeEmptyData() throws Exception {
        mockMvc.perform(post("/class")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"\"}"))
                .andExpect(status().isBadRequest());

        ArgumentCaptor<ClassCharacter> argumentCaptor = ArgumentCaptor.forClass(ClassCharacter.class);
        Mockito.verify(classService, Mockito.never()).createClass(argumentCaptor.capture());
    }
    ///

    //DELETE
    @Test
    void deleteClassPositive() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/class/" + EXISTS_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Mockito.verify(classService, Mockito.times(1)).deleteById(EXISTS_ID);
    }

    @Test
    void deleteClassNegativeNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/class/" + NOT_EXISTS_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        Mockito.verify(classService, Mockito.never()).deleteById(NOT_EXISTS_ID);
    }
    ///

    //UPDATE
    @Test
    void updateClassPositive() throws Exception {
        class_test_1.setName(UPDATED_NAME);
        Mockito.when(classService.existName(UPDATED_NAME)).thenReturn(false);
        Mockito.when(classService.getById(EXISTS_ID)).thenReturn(conv.toClassDTO(class_test_1));

        mockMvc.perform(MockMvcRequestBuilders.put("/class/" + EXISTS_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + UPDATED_NAME + "\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", Matchers.is(UPDATED_NAME)));

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<ClassCharacter> argumentCaptor = ArgumentCaptor.forClass(ClassCharacter.class);
        Mockito.verify(classService, Mockito.times(1)).updateById(idCaptor.capture(), argumentCaptor.capture());
        ClassCharacter dataProvidedToService = argumentCaptor.getValue();
        assertEquals(UPDATED_NAME, dataProvidedToService.getName());
        class_test_1.setName("CLASS_TEST_1");
    }

    @Test
    void updateClassNegativeNotFound() throws Exception {
        Mockito.when(classService.existName(UPDATED_NAME)).thenReturn(false);

        mockMvc.perform(put("/class/" + NOT_EXISTS_ID).
                        contentType(MediaType.APPLICATION_JSON).
                        content("{\"name\":\"" + UPDATED_NAME + "\"}")).
                andExpect(MockMvcResultMatchers.status().isNotFound());

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<ClassCharacter> argumentCaptor = ArgumentCaptor.forClass(ClassCharacter.class);
        Mockito.verify(classService, Mockito.never()).updateById(idCaptor.capture(), argumentCaptor.capture());
    }

    @Test
    void updateClassNegativeEmptyData() throws Exception {
        Mockito.when(classService.existName("")).thenReturn(false);
        mockMvc.perform(put("/class/" + EXISTS_ID).
                        contentType(MediaType.APPLICATION_JSON).
                        content("{\"name\":\"\"}")).
                andExpect(MockMvcResultMatchers.status().isBadRequest());

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<ClassCharacter> argumentCaptor = ArgumentCaptor.forClass(ClassCharacter.class);
        Mockito.verify(classService, Mockito.never()).updateById(idCaptor.capture(), argumentCaptor.capture());
    }

    @Test
    void updateClassNegativeExistsName() throws Exception {
        mockMvc.perform(put("/class/" + EXISTS_ID).
                        contentType(MediaType.APPLICATION_JSON).
                        content("{\"name\":\"" + ALREADY_EXISTS_NAME + "\"}")).
                andExpect(MockMvcResultMatchers.status().isBadRequest());

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<ClassCharacter> argumentCaptor = ArgumentCaptor.forClass(ClassCharacter.class);
        Mockito.verify(classService, Mockito.never()).updateById(idCaptor.capture(), argumentCaptor.capture());
    }
    ///
}