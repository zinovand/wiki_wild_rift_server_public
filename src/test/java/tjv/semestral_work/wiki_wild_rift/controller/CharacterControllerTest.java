package tjv.semestral_work.wiki_wild_rift.controller;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import tjv.semestral_work.wiki_wild_rift.dto.CharacterDTO;
import tjv.semestral_work.wiki_wild_rift.dto.Convertor;
import tjv.semestral_work.wiki_wild_rift.model.Character;
import tjv.semestral_work.wiki_wild_rift.service.CharacterService;
import tjv.semestral_work.wiki_wild_rift.service.ClassService;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CharacterController.class)
class CharacterControllerTest {

    //SET
    @MockBean
    CharacterService characterService;

    @MockBean
    ClassService classService;


    @Autowired
    MockMvc mockMvc;

    Character character_test_1 = new Character("CHARACTER_TEST_1", 112, 113, 1.14, 15, 16, 17, 18);
    Integer EXISTS_ID = 555;
    Integer NOT_EXISTS_ID = 123;
    String ALREADY_EXISTS_NAME = "ALREADY_EXISTS_NAME";
    String UPDATED_NAME = "UPDATED_NAME";
    Convertor conv = new Convertor();

    @BeforeEach
    void setId() {
        character_test_1.setId(EXISTS_ID);
        Mockito.when(characterService.existId(EXISTS_ID)).thenReturn(true);
        Mockito.when(characterService.existId(NOT_EXISTS_ID)).thenReturn(false);
        Mockito.when(characterService.existName(ALREADY_EXISTS_NAME)).thenReturn(true);
    }
    ///

    //GET ALL
    @Test
    void getAllCharacters() throws Exception {
        Character character_test_2 = new Character("CHARACTER_TEST_2", 222, 223, 2.24, 25, 26, 27, 28);
        character_test_2.setId(EXISTS_ID + 1);
        Set<CharacterDTO> allCharacters = new LinkedHashSet<>();
        allCharacters.add(conv.toCharacterDTO(character_test_1));
        allCharacters.add(conv.toCharacterDTO(character_test_2));

        Mockito.when(characterService.getAll()).thenReturn(allCharacters);

        mockMvc.perform(MockMvcRequestBuilders.get("/character")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(allCharacters.size())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is(character_test_1.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(character_test_1.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].health", Matchers.is(character_test_1.getHealth())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].move_speed", Matchers.is(character_test_1.getMove_speed())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].attack_speed", Matchers.is(character_test_1.getAttack_speed())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].physical_damage", Matchers.is(character_test_1.getPhysical_damage())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].magic_damage", Matchers.is(character_test_1.getMagic_damage())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].physical_armor", Matchers.is(character_test_1.getPhysical_armor())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].magic_armor", Matchers.is(character_test_1.getMagic_armor())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].health", Matchers.is(character_test_2.getHealth())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", Matchers.is(character_test_2.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].move_speed", Matchers.is(character_test_2.getMove_speed())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].attack_speed", Matchers.is(character_test_2.getAttack_speed())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].physical_damage", Matchers.is(character_test_2.getPhysical_damage())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].magic_damage", Matchers.is(character_test_2.getMagic_damage())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].physical_armor", Matchers.is(character_test_2.getPhysical_armor())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].magic_armor", Matchers.is(character_test_2.getMagic_armor())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", Matchers.is(character_test_2.getId())));
    }
    ///

    //GET ONE
    @Test
    void getOneCharacterPositive() throws Exception {
        Mockito.when(characterService.getById(EXISTS_ID)).thenReturn(conv.toCharacterDTO(character_test_1));
        mockMvc.perform(MockMvcRequestBuilders.get("/character/" + EXISTS_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is(character_test_1.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.health", Matchers.is(character_test_1.getHealth())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.move_speed", Matchers.is(character_test_1.getMove_speed())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.attack_speed", Matchers.is(character_test_1.getAttack_speed())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.physical_damage", Matchers.is(character_test_1.getPhysical_damage())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.magic_damage", Matchers.is(character_test_1.getMagic_damage())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.physical_armor", Matchers.is(character_test_1.getPhysical_armor())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.magic_armor", Matchers.is(character_test_1.getMagic_armor())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(character_test_1.getId())));
    }

    @Test
    void getOneCharacterNegativeNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/character/" + NOT_EXISTS_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        Mockito.verify(characterService, Mockito.never()).getById(NOT_EXISTS_ID);
    }
    ///

    //ADD NEW
    @Test
    void addCharacterPositive() throws Exception {
        Mockito.when(characterService.existName(character_test_1.getName())).thenReturn(false);
        Mockito.when(characterService.getByName(character_test_1.getName())).thenReturn(conv.toCharacterDTO(character_test_1));

        mockMvc.perform(post("/character")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + character_test_1.getName() + "\"," +
                                "\"health\":" + character_test_1.getHealth() + "," +
                                "\"move_speed\":" + character_test_1.getMove_speed() + "," +
                                "\"attack_speed\":" + character_test_1.getAttack_speed() + "," +
                                "\"physical_damage\":" + character_test_1.getPhysical_damage() + "," +
                                "\"magic_damage\":" + character_test_1.getMagic_damage() + "," +
                                "\"physical_armor\":" + character_test_1.getPhysical_armor() + "," +
                                "\"magic_armor\":" + character_test_1.getMagic_armor() + "}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.magic_armor", Matchers.is(character_test_1.getMagic_armor())))
                .andExpect(jsonPath("$.physical_armor", Matchers.is(character_test_1.getPhysical_armor())))
                .andExpect(jsonPath("$.magic_damage", Matchers.is(character_test_1.getMagic_damage())))
                .andExpect(jsonPath("$.physical_damage", Matchers.is(character_test_1.getPhysical_damage())))
                .andExpect(jsonPath("$.attack_speed", Matchers.is(character_test_1.getAttack_speed())))
                .andExpect(jsonPath("$.move_speed", Matchers.is(character_test_1.getMove_speed())))
                .andExpect(jsonPath("$.health", Matchers.is(character_test_1.getHealth())))
                .andExpect(jsonPath("$.name", Matchers.is(character_test_1.getName())));

        ArgumentCaptor<Character> argumentCaptor = ArgumentCaptor.forClass(Character.class);
        Mockito.verify(characterService, Mockito.times(1)).createCharacter(argumentCaptor.capture());
        Character dataProvidedToService = argumentCaptor.getValue();
        assertEquals(character_test_1.getName(), dataProvidedToService.getName());
    }

    @Test
    void addCharacterNegativeAlreadyExists() throws Exception {
        mockMvc.perform(post("/character")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + ALREADY_EXISTS_NAME + "\"," +
                                "\"health\":" + character_test_1.getHealth() + "," +
                                "\"move_speed\":" + character_test_1.getMove_speed() + "," +
                                "\"attack_speed\":" + character_test_1.getAttack_speed() + "," +
                                "\"physical_damage\":" + character_test_1.getPhysical_damage() + "," +
                                "\"magic_damage\":" + character_test_1.getMagic_damage() + "," +
                                "\"physical_armor\":" + character_test_1.getPhysical_armor() + "," +
                                "\"magic_armor\":" + character_test_1.getMagic_armor() + "}"))
                .andExpect(status().isBadRequest());

        ArgumentCaptor<Character> argumentCaptor = ArgumentCaptor.forClass(Character.class);
        Mockito.verify(characterService, Mockito.never()).createCharacter(argumentCaptor.capture());
    }

    @Test
    void addCharacterNegativeEmptyData() throws Exception {
        mockMvc.perform(post("/character")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"\"," +
                                "\"health\":" + character_test_1.getHealth() + "," +
                                "\"move_speed\":" + character_test_1.getMove_speed() + "," +
                                "\"attack_speed\":" + character_test_1.getAttack_speed() + "," +
                                "\"physical_damage\":" + character_test_1.getPhysical_damage() + "," +
                                "\"magic_damage\":" + character_test_1.getMagic_damage() + "," +
                                "\"physical_armor\":" + character_test_1.getPhysical_armor() + "," +
                                "\"magic_armor\":" + character_test_1.getMagic_armor() + "}"))
                .andExpect(status().isBadRequest());

        ArgumentCaptor<Character> argumentCaptor = ArgumentCaptor.forClass(Character.class);
        Mockito.verify(characterService, Mockito.never()).createCharacter(argumentCaptor.capture());
    }
    ///

    //DELETE
    @Test
    void deleteCharacterPositive() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/character/" + EXISTS_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Mockito.verify(characterService, Mockito.times(1)).deleteById(EXISTS_ID);
    }

    @Test
    void deleteCharacterNegativeNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/character/" + NOT_EXISTS_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        Mockito.verify(characterService, Mockito.never()).deleteById(NOT_EXISTS_ID);
    }
    ///

    //UPDATE
    @Test
    void updateCharacterPositive() throws Exception {
        character_test_1.setName(UPDATED_NAME);
        Mockito.when(characterService.existName(UPDATED_NAME)).thenReturn(false);
        Mockito.when(characterService.getById(EXISTS_ID)).thenReturn(conv.toCharacterDTO(character_test_1));

        mockMvc.perform(MockMvcRequestBuilders.put("/character/" + EXISTS_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + UPDATED_NAME + "\"," +
                                "\"health\":" + character_test_1.getHealth() + "," +
                                "\"move_speed\":" + character_test_1.getMove_speed() + "," +
                                "\"attack_speed\":" + character_test_1.getAttack_speed() + "," +
                                "\"physical_damage\":" + character_test_1.getPhysical_damage() + "," +
                                "\"magic_damage\":" + character_test_1.getMagic_damage() + "," +
                                "\"physical_armor\":" + character_test_1.getPhysical_armor() + "," +
                                "\"magic_armor\":" + character_test_1.getMagic_armor() + "}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", Matchers.is(UPDATED_NAME)));

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Character> argumentCaptor = ArgumentCaptor.forClass(Character.class);
        Mockito.verify(characterService, Mockito.times(1)).updateById(idCaptor.capture(), argumentCaptor.capture());
        Character dataProvidedToService = argumentCaptor.getValue();
        assertEquals(UPDATED_NAME, dataProvidedToService.getName());
        character_test_1.setName("CHARACTER_TEST_1");
    }

    @Test
    void updateCharacterNegativeNotFound() throws Exception {
        Mockito.when(characterService.existName(UPDATED_NAME)).thenReturn(false);

        mockMvc.perform(put("/character/" + NOT_EXISTS_ID).
                        contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + UPDATED_NAME + "\"," +
                                "\"health\":" + character_test_1.getHealth() + "," +
                                "\"move_speed\":" + character_test_1.getMove_speed() + "," +
                                "\"attack_speed\":" + character_test_1.getAttack_speed() + "," +
                                "\"physical_damage\":" + character_test_1.getPhysical_damage() + "," +
                                "\"magic_damage\":" + character_test_1.getMagic_damage() + "," +
                                "\"physical_armor\":" + character_test_1.getPhysical_armor() + "," +
                                "\"magic_armor\":" + character_test_1.getMagic_armor() + "}"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Character> argumentCaptor = ArgumentCaptor.forClass(Character.class);
        Mockito.verify(characterService, Mockito.never()).updateById(idCaptor.capture(), argumentCaptor.capture());
    }

    @Test
    void updateCharacterNegativeEmptyData() throws Exception {
        Mockito.when(characterService.getById(EXISTS_ID)).thenReturn(conv.toCharacterDTO(character_test_1));
        Mockito.when(characterService.existName("")).thenReturn(false);
        mockMvc.perform(put("/character/" + EXISTS_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"\"," +
                                "\"health\":" + character_test_1.getHealth() + "," +
                                "\"move_speed\":" + character_test_1.getMove_speed() + "," +
                                "\"attack_speed\":" + character_test_1.getAttack_speed() + "," +
                                "\"physical_damage\":" + character_test_1.getPhysical_damage() + "," +
                                "\"magic_damage\":" + character_test_1.getMagic_damage() + "," +
                                "\"physical_armor\":" + character_test_1.getPhysical_armor() + "," +
                                "\"magic_armor\":" + character_test_1.getMagic_armor() + "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Character> argumentCaptor = ArgumentCaptor.forClass(Character.class);
        Mockito.verify(characterService, Mockito.never()).updateById(idCaptor.capture(), argumentCaptor.capture());
    }

    @Test
    void updateCharacterNegativeExistsName() throws Exception {
        Mockito.when(characterService.existName(ALREADY_EXISTS_NAME)).thenReturn(true);
        Mockito.when(characterService.getById(EXISTS_ID)).thenReturn(conv.toCharacterDTO(character_test_1));
        mockMvc.perform(put("/character/" + EXISTS_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + ALREADY_EXISTS_NAME + "\"," +
                                "\"health\":" + character_test_1.getHealth() + "," +
                                "\"move_speed\":" + character_test_1.getMove_speed() + "," +
                                "\"attack_speed\":" + character_test_1.getAttack_speed() + "," +
                                "\"physical_damage\":" + character_test_1.getPhysical_damage() + "," +
                                "\"magic_damage\":" + character_test_1.getMagic_damage() + "," +
                                "\"physical_armor\":" + character_test_1.getPhysical_armor() + "," +
                                "\"magic_armor\":" + character_test_1.getMagic_armor() + "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Character> argumentCaptor = ArgumentCaptor.forClass(Character.class);
        Mockito.verify(characterService, Mockito.never()).updateById(idCaptor.capture(), argumentCaptor.capture());
    }
    ///

    //CHANGE CHARACTER CLASS
    @Test
    void changeClassPositive() throws Exception {
        Integer id_class = 777;
        Mockito.when(classService.existId(id_class)).thenReturn(true);
        mockMvc.perform(put("/character/" + EXISTS_ID + "/class").
                        contentType(MediaType.APPLICATION_JSON).
                        param("id_class", id_class.toString())).
                andExpect(MockMvcResultMatchers.status().isOk());

        ArgumentCaptor<Integer> idCharacterCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> idClassCaptor = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(characterService, Mockito.times(1)).updClassRequest(idCharacterCaptor.capture(), idClassCaptor.capture());
        assertEquals(EXISTS_ID, idCharacterCaptor.getValue());
        assertEquals(id_class, idClassCaptor.getValue());
    }

    @Test
    void changeClassNegativeNotFound() throws Exception {
        Integer id_class = 777;
        Mockito.when(classService.existId(id_class)).thenReturn(true);
        Mockito.when(classService.existId(NOT_EXISTS_ID)).thenReturn(false);

        mockMvc.perform(put("/character/" + NOT_EXISTS_ID + "/class").
                        contentType(MediaType.APPLICATION_JSON).
                        param("id_class", id_class.toString())).
                andExpect(MockMvcResultMatchers.status().isNotFound());

        ArgumentCaptor<Integer> idCharacterCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> idClassCaptor = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(characterService, Mockito.never()).updClassRequest(idCharacterCaptor.capture(), idClassCaptor.capture());

        mockMvc.perform(put("/character/" + EXISTS_ID + "/class").
                        contentType(MediaType.APPLICATION_JSON).
                        param("id_class", NOT_EXISTS_ID.toString())).
                andExpect(MockMvcResultMatchers.status().isNotFound());

        Mockito.verify(characterService, Mockito.never()).updClassRequest(idCharacterCaptor.capture(), idClassCaptor.capture());
    }
    ///

    //CHANGE CHARACTER COUNTERPICK
    @Test
    void changeCounterpicksPositive() throws Exception {
        Integer id_weak = EXISTS_ID + 1;
        Mockito.when(characterService.existId(id_weak)).thenReturn(true);

        mockMvc.perform(put("/counterpick").
                        contentType(MediaType.APPLICATION_JSON).
                        param("id_strong", EXISTS_ID.toString()).
                        param("id_weak", id_weak.toString())).
                andExpect(MockMvcResultMatchers.status().isOk());

        ArgumentCaptor<Integer> idStrongCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> idWeakCaptor = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(characterService, Mockito.times(1)).updCounterpicksRequest(idStrongCaptor.capture(), idWeakCaptor.capture());
        assertEquals(EXISTS_ID, idStrongCaptor.getValue());
        assertEquals(id_weak, idWeakCaptor.getValue());
    }

    @Test
    void changeCounterpicksNegativeNotFound() throws Exception {
        //first id not exists
        mockMvc.perform(put("/counterpick").
                        contentType(MediaType.APPLICATION_JSON).
                        param("id_strong", NOT_EXISTS_ID.toString()).
                        param("id_weak", EXISTS_ID.toString())).
                andExpect(MockMvcResultMatchers.status().isBadRequest());

        ArgumentCaptor<Integer> idStrongCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> idWeakCaptor = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(characterService, Mockito.never()).updCounterpicksRequest(idStrongCaptor.capture(), idWeakCaptor.capture());

        //second id not exists
        mockMvc.perform(put("/counterpick").
                        contentType(MediaType.APPLICATION_JSON).
                        param("id_strong", EXISTS_ID.toString()).
                        param("id_weak", NOT_EXISTS_ID.toString())).
                andExpect(MockMvcResultMatchers.status().isBadRequest());

        Mockito.verify(characterService, Mockito.never()).updCounterpicksRequest(idStrongCaptor.capture(), idWeakCaptor.capture());
    }
    ///
}