package tjv.semestral_work.wiki_wild_rift.controller;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import tjv.semestral_work.wiki_wild_rift.dto.Convertor;
import tjv.semestral_work.wiki_wild_rift.dto.SkillDTO;
import tjv.semestral_work.wiki_wild_rift.model.Skill;
import tjv.semestral_work.wiki_wild_rift.service.CharacterService;
import tjv.semestral_work.wiki_wild_rift.service.ClassService;
import tjv.semestral_work.wiki_wild_rift.service.SkillService;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SkillController.class)
class SkillControllerTest {

    //SET
    @MockBean
    SkillService skillService;

    @MockBean
    ClassService classService;

    @MockBean
    CharacterService characterService;

    @Autowired
    MockMvc mockMvc;

    Integer EXISTS_ID = 555;
    Integer NOT_EXISTS_ID = 123;
    String ALREADY_EXISTS_NAME = "ALREADY EXISTS NAME";
    Skill skill_test_1 = new Skill("SKILL_TEST_1", "EFFECT_TEST_1", 12, 13, 14, false);
    String UPDATED_NAME = "UPDATED_NAME";
    Convertor conv = new Convertor();

    @BeforeEach
    void setId() {
        skill_test_1.setId(EXISTS_ID);
        Mockito.when(skillService.existId(EXISTS_ID)).thenReturn(true);
        Mockito.when(skillService.existId(NOT_EXISTS_ID)).thenReturn(false);
        Mockito.when(skillService.existName(ALREADY_EXISTS_NAME)).thenReturn(true);
        Mockito.when(skillService.existEffect(ALREADY_EXISTS_NAME)).thenReturn(true);
    }
    ///

    //GET ALL
    @Test
    void getAllSkills() throws Exception {
        Skill skill_test_2 = new Skill("SKILL_TEST_2", "EFFECT_TEST_2", 22, 0, 0, true);
        skill_test_2.setId(EXISTS_ID + 1);
        Set<SkillDTO> allSkills = new LinkedHashSet<>();
        allSkills.add(conv.toSkillDTO(skill_test_1));
        allSkills.add(conv.toSkillDTO(skill_test_2));

        Mockito.when(skillService.getAll()).thenReturn(allSkills);

        mockMvc.perform(MockMvcRequestBuilders.get("/skill")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(allSkills.size())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is(skill_test_1.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].effect", Matchers.is(skill_test_1.getEffect())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(skill_test_1.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].cost", Matchers.is(skill_test_1.getCost())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].cooldown", Matchers.is(skill_test_1.getCooldown())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].action_time", Matchers.is(skill_test_1.getAction_time())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].is_passive", Matchers.is(skill_test_1.getIs_passive())))

                .andExpect(MockMvcResultMatchers.jsonPath("$[1].cost", Matchers.is(skill_test_2.getCost())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].effect", Matchers.is(skill_test_2.getEffect())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", Matchers.is(skill_test_2.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].cooldown", Matchers.is(skill_test_2.getCooldown())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].action_time", Matchers.is(skill_test_2.getAction_time())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].is_passive", Matchers.is(skill_test_2.getIs_passive())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", Matchers.is(skill_test_2.getId())));
    }

    @Test
    void getOneSkillPositive() throws Exception {
        Mockito.when(skillService.getById(EXISTS_ID)).thenReturn(conv.toSkillDTO(skill_test_1));
        mockMvc.perform(MockMvcRequestBuilders.get("/skill/" + EXISTS_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.cost", Matchers.is(skill_test_1.getCost())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.effect", Matchers.is(skill_test_1.getEffect())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is(skill_test_1.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cooldown", Matchers.is(skill_test_1.getCooldown())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.action_time", Matchers.is(skill_test_1.getAction_time())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.is_passive", Matchers.is(skill_test_1.getIs_passive())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(skill_test_1.getId())));
    }

    @Test
    void getOneSkillNegativeNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/skill/" + NOT_EXISTS_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        Mockito.verify(skillService, Mockito.never()).getById(NOT_EXISTS_ID);
    }
    ///

    //ADD NEW
    @Test
    void addSkillPositive() throws Exception {
        Mockito.when(skillService.existName(skill_test_1.getName())).thenReturn(false);
        Mockito.when(skillService.existEffect(skill_test_1.getEffect())).thenReturn(false);
        Mockito.when(skillService.getByName(skill_test_1.getName())).thenReturn(conv.toSkillDTO(skill_test_1));

        mockMvc.perform(post("/skill")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + skill_test_1.getName() + "\"," +
                                "\"effect\":\"" + skill_test_1.getEffect() + "\"," +
                                "\"action_time\":" + skill_test_1.getAction_time() + "," +
                                "\"cooldown\":" + skill_test_1.getCooldown() + "," +
                                "\"cost\":" + skill_test_1.getCost() + "," +
                                "\"is_passive\":" + skill_test_1.getIs_passive() + "}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", Matchers.is(skill_test_1.getName())))
                .andExpect(jsonPath("$.effect", Matchers.is(skill_test_1.getEffect())))
                .andExpect(jsonPath("$.action_time", Matchers.is(skill_test_1.getAction_time())))
                .andExpect(jsonPath("$.cooldown", Matchers.is(skill_test_1.getCooldown())))
                .andExpect(jsonPath("$.cost", Matchers.is(skill_test_1.getCost())))
                .andExpect(jsonPath("$.is_passive", Matchers.is(skill_test_1.getIs_passive())));

        ArgumentCaptor<Skill> argumentCaptor = ArgumentCaptor.forClass(Skill.class);
        Mockito.verify(skillService, Mockito.times(1)).createSkill(argumentCaptor.capture());
        Skill dataProvidedToService = argumentCaptor.getValue();
        assertEquals(skill_test_1.getName(), dataProvidedToService.getName());
    }

    @Test
    void addSkillNegativeAlreadyExists() throws Exception {
        Mockito.when(skillService.existEffect(skill_test_1.getEffect())).thenReturn(false);
        mockMvc.perform(post("/skill")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + ALREADY_EXISTS_NAME + "\"," +
                                "\"effect\":\"" + skill_test_1.getEffect() + "\"," +
                                "\"action_time\":" + skill_test_1.getAction_time() + "," +
                                "\"cooldown\":" + skill_test_1.getCooldown() + "," +
                                "\"cost\":" + skill_test_1.getCost() + "," +
                                "\"is_passive\":" + skill_test_1.getIs_passive() + "}"))
                .andExpect(status().isBadRequest());

        ArgumentCaptor<Skill> argumentCaptor = ArgumentCaptor.forClass(Skill.class);
        Mockito.verify(skillService, Mockito.never()).createSkill(argumentCaptor.capture());

        Mockito.when(skillService.existName(skill_test_1.getName())).thenReturn(false);
        mockMvc.perform(post("/skill")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + skill_test_1.getName() + "\"," +
                                "\"effect\":\"" + ALREADY_EXISTS_NAME + "\"," +
                                "\"action_time\":" + skill_test_1.getAction_time() + "," +
                                "\"cooldown\":" + skill_test_1.getCooldown() + "," +
                                "\"cost\":" + skill_test_1.getCost() + "," +
                                "\"is_passive\":" + skill_test_1.getIs_passive() + "}"))
                .andExpect(status().isBadRequest());

        Mockito.verify(skillService, Mockito.never()).createSkill(argumentCaptor.capture());
    }

    @Test
    void addSkillNegativeEmptyData() throws Exception {
        Mockito.when(skillService.existName(skill_test_1.getName())).thenReturn(false);
        Mockito.when(skillService.existEffect(skill_test_1.getEffect())).thenReturn(false);
        mockMvc.perform(post("/skill")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"\"," +
                                "\"effect\":\"" + ALREADY_EXISTS_NAME + "\"," +
                                "\"action_time\":" + skill_test_1.getAction_time() + "," +
                                "\"cooldown\":" + skill_test_1.getCooldown() + "," +
                                "\"cost\":" + skill_test_1.getCost() + "," +
                                "\"is_passive\":" + skill_test_1.getIs_passive() + "}"))
                .andExpect(status().isBadRequest());

        ArgumentCaptor<Skill> argumentCaptor = ArgumentCaptor.forClass(Skill.class);
        Mockito.verify(skillService, Mockito.never()).createSkill(argumentCaptor.capture());

        Mockito.when(skillService.existEffect(skill_test_1.getEffect())).thenReturn(false);
        mockMvc.perform(post("/skill")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + ALREADY_EXISTS_NAME + "\"," +
                                "\"effect\":\"\"," +
                                "\"action_time\":" + skill_test_1.getAction_time() + "," +
                                "\"cooldown\":" + skill_test_1.getCooldown() + "," +
                                "\"cost\":" + skill_test_1.getCost() + "," +
                                "\"is_passive\":" + skill_test_1.getIs_passive() + "}"))
                .andExpect(status().isBadRequest());

        Mockito.verify(skillService, Mockito.never()).createSkill(argumentCaptor.capture());
    }
    ///

    //DELETE
    @Test
    void deleteSkillPositive() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/skill/" + EXISTS_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Mockito.verify(skillService, Mockito.times(1)).deleteById(EXISTS_ID);
    }

    @Test
    void deleteSkillNegativeNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/skill/" + NOT_EXISTS_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        Mockito.verify(skillService, Mockito.never()).deleteById(NOT_EXISTS_ID);
    }
    ///

    //UPDATE
    @Test
    void updateSkillPositive() throws Exception {
        skill_test_1.setName(UPDATED_NAME);
        Mockito.when(skillService.existName(UPDATED_NAME)).thenReturn(false);
        Mockito.when(skillService.getById(EXISTS_ID)).thenReturn(conv.toSkillDTO(skill_test_1));

        mockMvc.perform(MockMvcRequestBuilders.put("/skill/" + EXISTS_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + UPDATED_NAME + "\"," +
                                "\"effect\":\"" + skill_test_1.getEffect() + "\"," +
                                "\"action_time\":" + skill_test_1.getAction_time() + "," +
                                "\"cooldown\":" + skill_test_1.getCooldown() + "," +
                                "\"cost\":" + skill_test_1.getCost() + "," +
                                "\"is_passive\":" + skill_test_1.getIs_passive() + "}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", Matchers.is(UPDATED_NAME)));

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Skill> argumentCaptor = ArgumentCaptor.forClass(Skill.class);
        Mockito.verify(skillService, Mockito.times(1)).updateById(idCaptor.capture(), argumentCaptor.capture());
        Skill dataProvidedToService = argumentCaptor.getValue();
        assertEquals(UPDATED_NAME, dataProvidedToService.getName());
        skill_test_1.setName("CHARACTER_TEST_1");
    }

    @Test
    void updateSkillNegativeNotFound() throws Exception {
        Mockito.when(skillService.existName(UPDATED_NAME)).thenReturn(false);

        mockMvc.perform(put("/skill/" + NOT_EXISTS_ID).
                        contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + UPDATED_NAME + "\"," +
                                "\"effect\":\"" + skill_test_1.getEffect() + "\"," +
                                "\"action_time\":" + skill_test_1.getAction_time() + "," +
                                "\"cooldown\":" + skill_test_1.getCooldown() + "," +
                                "\"cost\":" + skill_test_1.getCost() + "," +
                                "\"is_passive\":" + skill_test_1.getIs_passive() + "}"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Skill> argumentCaptor = ArgumentCaptor.forClass(Skill.class);
        Mockito.verify(skillService, Mockito.never()).updateById(idCaptor.capture(), argumentCaptor.capture());
    }

    @Test
    void updateSkillNegativeEmptyData() throws Exception {
        Mockito.when(skillService.existName(UPDATED_NAME)).thenReturn(false);
        Mockito.when(skillService.existName(skill_test_1.getEffect())).thenReturn(false);
        mockMvc.perform(put("/skill/" + EXISTS_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"\"," +
                                "\"effect\":\"" + skill_test_1.getEffect() + "\"," +
                                "\"action_time\":" + skill_test_1.getAction_time() + "," +
                                "\"cooldown\":" + skill_test_1.getCooldown() + "," +
                                "\"cost\":" + skill_test_1.getCost() + "," +
                                "\"is_passive\":" + skill_test_1.getIs_passive() + "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Skill> argumentCaptor = ArgumentCaptor.forClass(Skill.class);
        Mockito.verify(skillService, Mockito.never()).updateById(idCaptor.capture(), argumentCaptor.capture());

        mockMvc.perform(put("/skill/" + EXISTS_ID).
                        contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + UPDATED_NAME + "\"," +
                                "\"effect\":\"\"," +
                                "\"action_time\":" + skill_test_1.getAction_time() + "," +
                                "\"cooldown\":" + skill_test_1.getCooldown() + "," +
                                "\"cost\":" + skill_test_1.getCost() + "," +
                                "\"is_passive\":" + skill_test_1.getIs_passive() + "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
        Mockito.verify(skillService, Mockito.never()).updateById(idCaptor.capture(), argumentCaptor.capture());
    }

    @Test
    void updateSkillNegativeExistsNameOrEffect() throws Exception {
        Mockito.when(skillService.getById(EXISTS_ID)).thenReturn(conv.toSkillDTO(skill_test_1));
        mockMvc.perform(put("/skill/" + EXISTS_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + ALREADY_EXISTS_NAME + "\"," +
                                "\"effect\":\"" + skill_test_1.getEffect() + "\"," +
                                "\"action_time\":" + skill_test_1.getAction_time() + "," +
                                "\"cooldown\":" + skill_test_1.getCooldown() + "," +
                                "\"cost\":" + skill_test_1.getCost() + "," +
                                "\"is_passive\":" + skill_test_1.getIs_passive() + "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Skill> argumentCaptor = ArgumentCaptor.forClass(Skill.class);
        Mockito.verify(skillService, Mockito.never()).updateById(idCaptor.capture(), argumentCaptor.capture());

        mockMvc.perform(put("/skill/" + EXISTS_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + skill_test_1.getName() + "\"," +
                                "\"effect\":\"" + ALREADY_EXISTS_NAME + "\"," +
                                "\"action_time\":" + skill_test_1.getAction_time() + "," +
                                "\"cooldown\":" + skill_test_1.getCooldown() + "," +
                                "\"cost\":" + skill_test_1.getCost() + "," +
                                "\"is_passive\":" + skill_test_1.getIs_passive() + "}"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        Mockito.verify(skillService, Mockito.never()).updateById(idCaptor.capture(), argumentCaptor.capture());
    }
    ///

    //CHANGE SKILL OWNER
    @Test
    void changeSkillOwnerPositive() throws Exception {
        Integer id_owner = 777;
        Mockito.when(characterService.existId(id_owner)).thenReturn(true);
        mockMvc.perform(put("/skill/" + EXISTS_ID + "/character").
                        contentType(MediaType.APPLICATION_JSON).
                        param("id_owner", id_owner.toString())).
                andExpect(MockMvcResultMatchers.status().isOk());

        ArgumentCaptor<Integer> idClassCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> idOwnerCaptor = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(skillService, Mockito.times(1)).updSkillOwner(idClassCaptor.capture(), idOwnerCaptor.capture());
        assertEquals(EXISTS_ID, idClassCaptor.getValue());
        assertEquals(id_owner, idOwnerCaptor.getValue());
    }

    @Test
    void changeSkillOwnerNegativeNotFound() throws Exception {
        Integer id_owner = 777;
        mockMvc.perform(put("/skill/" + NOT_EXISTS_ID + "/character").
                        contentType(MediaType.APPLICATION_JSON).
                        param("id_owner", id_owner.toString())).
                andExpect(MockMvcResultMatchers.status().isNotFound());

        ArgumentCaptor<Integer> idSkillCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> idOwnerCaptor = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(skillService, Mockito.never()).updSkillOwner(idSkillCaptor.capture(), idOwnerCaptor.capture());
    }
    ///

    //ALL SKILLS FOR CLASS
    @Test
    void getAllSkillForClassPositive() throws Exception {
        Skill skill_test_2 = new Skill("SKILL_TEST_2", "EFFECT_TEST_2", 22, 0, 0, true);
        skill_test_2.setId(EXISTS_ID + 1);
        Set<SkillDTO> allSkillsForClass = new LinkedHashSet<>();
        allSkillsForClass.add(conv.toSkillDTO(skill_test_1));
        allSkillsForClass.add(conv.toSkillDTO(skill_test_2));
        String class_name_test = "CLASS_NAME_TEST";

        Mockito.when(classService.existName(class_name_test)).thenReturn(true);
        Mockito.when(skillService.getAllSkillForClass(class_name_test)).thenReturn(allSkillsForClass);

        mockMvc.perform(MockMvcRequestBuilders.get("/skill/for_class")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("class_name", class_name_test))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(allSkillsForClass.size())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is(skill_test_1.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].effect", Matchers.is(skill_test_1.getEffect())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(skill_test_1.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].cost", Matchers.is(skill_test_1.getCost())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].cooldown", Matchers.is(skill_test_1.getCooldown())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].action_time", Matchers.is(skill_test_1.getAction_time())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].is_passive", Matchers.is(skill_test_1.getIs_passive())))

                .andExpect(MockMvcResultMatchers.jsonPath("$[1].cost", Matchers.is(skill_test_2.getCost())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].effect", Matchers.is(skill_test_2.getEffect())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", Matchers.is(skill_test_2.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].cooldown", Matchers.is(skill_test_2.getCooldown())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].action_time", Matchers.is(skill_test_2.getAction_time())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].is_passive", Matchers.is(skill_test_2.getIs_passive())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", Matchers.is(skill_test_2.getId())));
    }

    @Test
    void getAllSkillForClassNegativeClassNameNotFound() throws Exception {
        String class_name_test = "CLASS_NAME_TEST";
        Mockito.when(classService.existName(class_name_test)).thenReturn(false);

        mockMvc.perform(MockMvcRequestBuilders.get("/skill/for_class")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("class_name", class_name_test))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
        Mockito.verify(skillService, Mockito.never()).getAllSkillForClass(any(String.class));
    }
    ///

}